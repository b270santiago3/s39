const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/



module.exports.addCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin) {

		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots,
			createdOn: req.body.createdOn,
			enrollees: []
		});

		// Saves the created object to our database
		return newCourse.save()
		// Course creation is successful
		.then(course => {
			console.log(course);
			res.send(true);
		})
		// Course creation failed
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
	
}


module.exports.getAllCourses = (req,res) => {
	return Course.find({}).then(result => res.send(result));
}

//Retrieve all Active courses
/*
	Step:
	1. Retrieve all the courses from the database with property of "isActive: true"
*/

module.exports.getAllActive = (req,res) => {

	return Course.find({isActive: true}).then(result => res.send(result));
}


// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (req, res) => {

	console.log(req.params.id)

	return Course.findById(req.params.id)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}

 // Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		slots: req.body.slots
		}

		// Syntax : findByIdAndUpdate(documentId, update)
		return Course.findByIdAndUpdate(req.params.courseId, updateCourse)
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
		})
	} else {
		return res.send(false);	
	}
}	

module.exports.archiveCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const token = req.headers.authorization.split(' ')[1];
	

	if (userData.isActive) {
		const courseId = req.params.courseId;
		const result = Course.findByIdAndDelete(courseId);

		if (result) {
			console.log(result);
			return res.send(result);

		} else {
			return res.send(false);
		}

	} 
}

module.exports.addCourse = (req, res) => {
	try {
		if (!req.user.isAdmin) {
			return res.status(401).json({ message: 'Unauthorized' });
		}

		const course = new Course({
			title: req.body.title,
			description: req.body.description,
			price: req.body.price

		});

		course.save();

		res.status(201).json({ message: 'Course created successfully' });

	} catch (error) {

		console.error(error);
		res.status(500).json({ message: 'Server Error' });

	}
};






